from sqlalchemy import Column, Integer, String,DateTime,ForeignKey,Float
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import datetime
from db import Base

class TransactionHistory(Base):
    __tablename__ = "transactionHistory"
    id = Column(Integer, primary_key=True)
    date = Column(DateTime,default=datetime.datetime.now())
    event = Column(String, nullable=False)
    pointChange = Column(Float, default=0)
    clubId = Column(Integer, ForeignKey("club_entities.id"))
    club = relationship("ClubEntity",back_populates="transactions")

