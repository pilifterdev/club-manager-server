from email.policy import default
from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import validates,relationship
from sqlalchemy.ext.hybrid import hybrid_property
import enum
import uuid

from db import Base

class ClubType(enum.Enum):
    INDIVIDUAL = "Individual"
    CLUB = "Club"

class ClubStatus(enum.Enum):
    ACTIVE = "Active"
    DORMANT =  "Dormant"

class ClubEntity(Base):
    __tablename__ = 'club_entities'

    id = Column(Integer, primary_key=True)
    name = Column(String,unique=True,nullable=True)
    type = Column(Enum(ClubType),default="CLUB")
    status = Column(Enum(ClubStatus),default="ACTIVE")
    email = Column(String, nullable=False)


    transactions = relationship("TransactionHistory",back_populates="club")
    @hybrid_property
    def points(self):
        return sum([transaction.pointChange for transaction in self.transactions])
    @validates('email')
    def checkEmail(self, key, email):
        assert "@" in email, "Invalid email"
        return email
