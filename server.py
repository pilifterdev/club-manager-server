from locale import currency
from typing import List
from fastapi import FastAPI
import db,models,schemas
from fastapi.middleware.cors import CORSMiddleware
import stripe
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from jinja2 import Environment, PackageLoader, select_autoescape
from dotenv import load_dotenv

load_dotenv()

env = Environment(
    loader=PackageLoader("server"),
    autoescape=select_autoescape()
)

stripe.api_key = "sk_live_okhLfpzdeTMNu7UcOUNtcAxZ00GDtGQ0iL"
origins = [
    "http://localhost",
    "http://localhost:8080",
    "https://clubs.gbpflondon.org",
    "https://localhost"
]
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
session = db.SessionLocal()
@app.get('/')
async def index():
    return {'status':'OK'}

@app.get('/clubs',response_model=List[schemas.ClubEntity])
async def clubs():
    return  session.query(models.ClubEntity).order_by(models.ClubEntity.status).order_by(models.ClubEntity.name).all()
    

@app.get('/transactions',response_model=List[schemas.TransactionHistory])
async def transactions():
    return session.query(models.TransactionHistory).order_by(models.TransactionHistory.date.desc()).all()

@app.get('/payment/{payment_total}')
async def payment(payment_total:int):
    return stripe.PaymentIntent.create(amount=payment_total,currency='gbp',payment_method_types=["card"])

@app.post('/email')
async def sendEmail(club:schemas.Club,athletes:List[schemas.Athlete]):
    actualClub = session.query(models.ClubEntity).filter(models.ClubEntity.name==club.name).first()
    template = env.get_template("confirmation.html")
    message = Mail(
        from_email = 'dom@gbpflondon.org',
        to_emails = ['dom@gbpflondon.org',actualClub.email],
        subject = 'Early Entry-Confirmation',
        html_content = template.render(club=club,athletes=athletes)
    )
    try:
         sg= SendGridAPIClient(os.environ['SENDGRID_API_KEY'])
         response = sg.send(message)
    except Exception as e:
        print("Unable to send mail")
        print(e)

