from db import SessionLocal
from models import *
# from sqlalchemy import event

session = SessionLocal()


clubs = [
    ClubEntity(name="Built Differently", email="info@builtdifferently.co.uk"),
    ClubEntity(name="Strength Ambassadors",
               email="sally@strengthambassadors.com"),
    ClubEntity(name="British Barbell", email="paul@marshcoaching.co.uk"),
    ClubEntity(name="Bethnal Green Weightlifting Club",
               email="bbasshouse@aol.com"),
    ClubEntity(name="AIPP", email="jurinskengamu@gmail.com"),
    ClubEntity(name="UCL Barbell Club", email="uclubarbell@gmail.com"),
    ClubEntity(name="Crystal Palace Powerlifting Club", email="unkown@email.com"),
    ClubEntity(name="Physical Culture", email="robrees@hotmail.com"),
    ClubEntity(name="P10 Fitness", email="paul@marshcoaching.co.uk"),
    ClubEntity(name="IC Barbell", status="DORMANT",email="unknown@email.com"),
    ClubEntity(name="Queen Mary", status="DORMANT",email="unkown@email.com"),
    ClubEntity(name="St. Mary's", status="DORMANT",email="unkonwn@email.com"),
    ClubEntity(name="Merton Weightlifting Club", email="info@mwlclub.com"),
    ClubEntity(name="Greenwich Powerlifting Club",
               email="acos.ashioti@me.com"),
    ClubEntity(name="Feral Strength", status="DORMANT",email="feralstrength.oc@gmail.com"),
    ClubEntity(name="Malcolm Long", type="INDIVIDUAL",email="unkown@email.com"),
    ClubEntity(name="Chris Gillard",type="INDIVIDUAL",email="unkown@email.com"),
    ClubEntity(name="Gus the Strength Coach", type="INDIVIDUAL",email="unknown@email.com")
]
session.bulk_save_objects(clubs)
session.commit()
session.query(ClubEntity).count()

