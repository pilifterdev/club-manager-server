from datetime import datetime
import models
from typing import Optional, List
import typing
from uuid import UUID

from sqlalchemy.sql.sqltypes import Date
from pydantic import BaseModel
from models.clubEntity import ClubType,ClubStatus


class TransactionHistoryForClubs(BaseModel):
    id:int
    date:datetime
    event:str
    pointChange:float

    class Config:
        orm_mode=True

class ClubEntity(BaseModel):
    id:int
    name:str
    type:ClubType
    status:ClubStatus
    points:float
    transactions:List[TransactionHistoryForClubs]

    class Config:
        orm_mode=True



class ClubEntityForTransactions(BaseModel):
    id:int
    name:str

    class Config:
        orm_mode=True

class TransactionHistory(BaseModel):
    id:int
    date:datetime
    event:str
    pointChange:float
    club:ClubEntityForTransactions


    class Config:
        orm_mode=True

class User(BaseModel):
    name:str
    email:str

class Club(BaseModel):
    name:str

class Athlete(BaseModel):
    firstName:str
    lastName:str
    weightClass:str
    contactInfo:str
