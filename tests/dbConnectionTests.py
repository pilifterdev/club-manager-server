import db
import pytest
from models import (ClubEntity)

class TestDBConnection:

    def testICanGetTestDBConnection(self):
        assert "sql" in db.SQLALCHEMY_DATABASE_URL
        self.session =  db.SessionLocal()
        assert self.session is not None
    
    def testICanInteractWithTheDB(self,testDB):
        assert testDB.query(ClubEntity).count() == 0
    


