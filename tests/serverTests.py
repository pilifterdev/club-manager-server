from fastapi import FastAPI
from fastapi.testclient import TestClient
from models import (ClubEntity)
import db

from server import app



testServer = TestClient(app)

def testICanReachAServer():
    response = testServer.get('/')
    assert response.status_code==200

def testThatICanGetAListOfClubs(testDB):
    response = testServer.get('/clubs')
    assert response.status_code == 200

def testThatICanSendAConfirmationEmailWithTheCorrectInformation(testDB):
    goodClub = ClubEntity(name="A Club", email="some@email.com")
    testDB.add(goodClub)
    testDB.commit()
    response = testServer.post('/email',json={'club':{'name':'A Club'},'athletes':[{'firstName':'Something','lastName':'Others','weightClass':'AnotherThings','contactInfo':'smethingmore'}]})
    assert response.status_code == 200