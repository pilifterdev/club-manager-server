import pytest
import db


@pytest.fixture(scope="module")
def testDB():
    print("Setting up database")
    db.Base.metadata.create_all(bind=db.engine)
    session = db.SessionLocal()
    yield session
    print("Closing database")
    session.close()
    db.Base.metadata.drop_all(bind=db.engine)

