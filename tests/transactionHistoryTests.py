from models import (ClubEntity,TransactionHistory)

def testWhenICreateATransactionIAlsoUpdateTheRelevantClub(testDB):
    assert testDB.query(TransactionHistory).count()==0
    sampleClub = ClubEntity(name="sample club",email="my@email.com")
    sampleTransaction = TransactionHistory(event="initial entry",pointChange=10)
    sampleTransaction.club=sampleClub
    testDB.add(sampleTransaction)
    testDB.add(sampleClub)
    testDB.commit()
    assert testDB.query(TransactionHistory).count()==1
    assert len(sampleClub.transactions)==1
