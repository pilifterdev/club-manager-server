from models.transactionHistory import TransactionHistory
import db
import pytest
import sqlalchemy
import uuid
import os
from models import (ClubEntity)


def testICanOnlyInsertWellBehavedClubsOrEntities(testDB):
    assert testDB.query(ClubEntity).count() == 0
    with pytest.raises(AssertionError):
        badClub = ClubEntity(name="My own club", email="email.com")
        testDB.add(badClub)
        testDB.commit()
    testDB.rollback()
    goodClub = ClubEntity(name="My own club", email="some@email.com")
    testDB.add(goodClub)
    testDB.commit()
    assert testDB.query(ClubEntity).count() == 1

def testICanCorrectlySumPointsFromTransactions(testDB):
    club = testDB.query(ClubEntity).first()
    assert club.points==0
    club.transactions.append(TransactionHistory(event='sample insert',pointChange=10))
    club.transactions.append(TransactionHistory(event='another insert',pointChange=-6))
    testDB.commit()
    assert club.points == 4
    assert testDB.query(TransactionHistory).count()==2
